#
#  Be sure to run `pod spec lint Infinea.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|
  spec.name         = "Infinea"
  spec.version      = "0.0.3"
  spec.summary      = "Infinea SDK"
  spec.homepage     = "http://accenture.com"
  spec.license       = { :type => 'MIT', :file => 'LICENSE' }
  spec.author       = { "Lamungkun, Sarunyoo" => "sarunyoo.lamungkun@accenture.com" }
  spec.source       = { :git => "https://gitlab.com/dearboy15/infineasdk.git", :tag => "#{spec.version}" }
  spec.ios.deployment_target = "10.0"
  spec.compiler_flags      = "-ObjC"
  spec.source_files = 'Infinea'
# spec.public_header_files = "Infinea/InfineaSDK.framework/Headers/*.h"
  spec.vendored_frameworks = 'InfineaSDK.framework'
  spec.frameworks = "CoreData", "ExternalAccessory", "Foundation"
end
